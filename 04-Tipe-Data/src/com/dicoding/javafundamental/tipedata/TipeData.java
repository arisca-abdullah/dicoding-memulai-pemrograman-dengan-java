package com.dicoding.javafundamental.tipedata;

public class TipeData {

    public static void main(String[] args) {
        // Byte
        byte a = 10;
        byte b = -10;
        System.out.println(a);
        System.out.println(b);

        // Short
        short c = 15000;
        short d = -20000;
        System.out.println(c);
        System.out.println(d);

        // Integer
        int e = 150000;
        int f = -200000;
        System.out.println(e);
        System.out.println(f);

        // Long
        long g = 150000L;
        long h = -200000L;
        System.out.println(g);
        System.out.println(h);

        // Float
        float i = 3.5f;
        System.out.println(i);

        // Double
        double j = 5.0;
        System.out.println(j);

        // Boolean
        boolean k = true;
        boolean l = false;
        System.out.println(k);
        System.out.println(l);

        // Char
        char item = 'A';
        System.out.println(item);
    }

}
