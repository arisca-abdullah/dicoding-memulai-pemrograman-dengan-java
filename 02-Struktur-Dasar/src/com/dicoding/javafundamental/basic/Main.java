package com.dicoding.javafundamental.basic;

import com.dicoding.javafundamental.basic.hewan.Kambing;
import com.dicoding.javafundamental.basic.kendaraan.Kereta;
import com.dicoding.javafundamental.basic.kendaraan.Mobil;
import com.dicoding.javafundamental.basic.kendaraan.Motor;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello, World!");

        Kambing.bunyi();

        Mobil.jumlahBan();
        Motor.jumlahBan();
        Kereta.jumlahBan();

        Date today = new Date();
        System.out.println("Hari ini = " + today);
    }
}
